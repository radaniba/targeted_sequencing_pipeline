# Args:
#   [1] : REF_GENOME - Path to reference genome BAM file was aligned to
#   [2] : FASTQ_1 - First FASTQ with paired end data
#   [3] : FASTQ_2 - Second FASTQ with paired end data
#   [4] : OUT_FILE - Path where output will be written in BAM format

REF_GENOME=$1

FASTQ_1=$2

FASTQ_2=$3

OUT_FILE=$4

#bwa mem $REF_GENOME $FASTQ_1 $FASTQ_2 | samtools view -bS - > $OUT_FILE


#bwa aln $REF_GENOME $FASTQ_1 > $FASTQ_1.fai
#bwa aln $REF_GENOME $FASTQ_2 > $FASTQ_2.fai

#a = basename $FASTQ_1
#b = basename $FASTQ_2
#dir = dirname $FASTQ_1

#bwa sampe $REF_GENOME $FASTQ_1.fai $FASTQ_2.fai $FASTQ_1 $FASTQ_2 > $dir/$a$b.sam
#samtools view -S $dir/$a$b.sam | samtools view -bS -T $ref_GENOME -> $OUT_FILE
#rm $FASTQ_1.fai $FASTQ_2.fai $dir/$a$b.sam
