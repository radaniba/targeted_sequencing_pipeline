from smtplib import SMTP
from smtplib import SMTPException
from email.mime.text import MIMEText


######### Notify the user that everythong went smoothly, in case the user chose to be notified #########

EMAIL_SUBJECT = "You successfully called variants on your data"
#EMAIL_RECEIVERS = ""
EMAIL_SENDER  =  'deepseq.pipeline@gmail.com'
GMAIL_SMTP = "smtp.gmail.com"
GMAIL_SMTP_PORT = 587
TEXT_SUBTYPE = "plain"

def listToStr(lst):
    """This method makes comma separated list item string"""
    return ','.join(lst)

def notify_user(EMAIL_RECEIVERS, subject,content, pswd):
    """This method sends an email"""

    #Create the message
    msg = MIMEText(content, TEXT_SUBTYPE)
    msg["Subject"] = subject
    msg["From"] = EMAIL_SENDER
    msg["To"] = EMAIL_RECEIVERS

    try:
      smtpObj = SMTP(GMAIL_SMTP, GMAIL_SMTP_PORT)
      #Identify yourself to GMAIL ESMTP server.
      smtpObj.ehlo()
      #Put SMTP connection in TLS mode and call ehlo again.
      smtpObj.starttls()
      smtpObj.ehlo()
      #Login to service
      smtpObj.login(user=EMAIL_SENDER, password=pswd)
      #Send email
      smtpObj.sendmail(EMAIL_SENDER, EMAIL_RECEIVERS, msg.as_string())
      #close connection and session.
      smtpObj.quit();
    except SMTPException as error:
      print "Error: unable to send email :  {err}".format(err=error)

######## end notification ###################
