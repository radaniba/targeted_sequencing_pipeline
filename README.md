# MiSeq Analysis Pipeline

## Versions

### 0.1.0

First commit for the Shah lab.

## About

This pipeline is designed to analyse a targeted panel of mutations from one or more samples. If a normal control sample
is specified in the `config.yaml` file, somatic mutations will be called.

## Dependencies

Note the exact versions tested are given for each package. Other versions may work.

* [bwa == 0.7.5a](http://bio-bwa.sourceforge.net)

* [Python == 2.7.5](http://www.python.org)

* [samtools == 0.1.19](http://samtools.sourceforge.net)

### Python Libraries

* [numpy == 1.7.1](http://www.numpy.org)

* [pandas == 0.12.0](http://pandas.pydata.org)

* [pysam == 0.7.5](http://code.google.com/p/pysam)

* [PyYAML == 3.10](http://pyyaml.org)

* [ruffus == 2.3b1](http://www.ruffus.org.uk)

* [scipy == 0.12.0](http://www.scipy.org)

* [statsmodels == 0.5.0](https://pypi.python.org/pypi/statsmodels)

* [xlrd == 0.9.2](http://www.python-excel.org)

* [xlwt == 0.7.5](http://www.python-excel.org)

### Optional Python Libraries

For running on a cluster you will need to install

* [drmaa-python](http://drmaa-python.github.io)

## Installation

Check out the latest code from the [BitBucket repository](https://bitbucket.org/aroth85/pipelines).

## Running

To run the pipeline enter the single_cell/code directory from folder checked out above. To execute the pipeline use the following


```
python pipeline config.yaml --num_cpus NUM_CPUS --mode MODE --install_dir INSTALL_DIR
```


The arguments are as follows

* NUM_CPUS - Number of concurrent jobs to run. If run locally these number of cpus will be used, and if run on a cluster this number of jobs will be launched. Note that if the number of cpus is less than the number of samples in the config.yaml file, jobs will be queued. For clusters its best to set this higher then the number of samples and let the queue manager deal with scheduling.

* MODE - There are three options

	* printout - Will print the tasks which need to be run.

	* local - Will run all jobs locally.

	* cluster - Will launch the jobs on a SGE cluster. This assumes you are executing the job from a head node.

* INSTALL_DIR - A path to directory containing local installations of your software. If this is ommitted only programs and libraries in the global path will be executed.

## Input Files

There are example input files in the examples/ folder. There are two key files the pipeline needs to run.

* config.yaml - This file specifies the details of the analysis and samples. This file is in YAML format.

* positions.tsv - This is tab separated file with a header. It lists the target positions for the analysis. The fields in this file are

	* chrom - The chromosome of the locus. This must match the convention of the reference genome aligned to i.e. chr1 instead of 1 if you use the UCSC genome.

	* coord - The chromosome coordinate of the locus. This is 1-based.

	* ref_base - The reference allele for this position.

	* var_base - The variant allele for this position.

	* amplicon_beg - The beginning coordiante of the amplicon.

	* amplicon_end - The ending coordinate for the the amplicon.

## Output

All files will be output under the folder specified in the config.yaml folder. Intermediary files such as BAM files will be placed in the tmp/ folder, tables under tables/ and plots under plots/.
