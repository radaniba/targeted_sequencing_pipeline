from __future__ import division

from statsmodels.sandbox.stats.multicomp import multipletests

import csv
import pandas as pd
import numpy as np
import scipy as sp

bases = ['A', 'C', 'G', 'T']

fields = [
          'chrom',
          'coord',
          'ref_base',
          'var_base',
          'ref_counts',
          'var_counts',
          'background_average_var_freq',
          'var_freq',
          'p_value'
          ]

def main(args):
    counts = pd.read_csv(args.counts_file, sep='\t')
    
    counts['chrom'] = counts['chrom'].astype(str)
    
    positions = pd.read_csv(args.positions_file, sep='\t')
    
    data = []

    for _, row in positions.iterrows():
        chrom = row['chrom']
        
        coord = row['coord']
        
        amplicon_beg = row['amplicon_beg']
        
        amplicon_end = row['amplicon_end']
                
        ref_base = row['ref_base']
        
        var_base = row['var_base']
        
        if var_base not in bases:
            continue
        
        if (coord < amplicon_beg) or (coord > amplicon_end):
            print 'Skipping {0}:{1} because it is not contained in amplicon {2}-{3}.'.format(chrom, 
                                                                                             coord, 
                                                                                             amplicon_beg, 
                                                                                             amplicon_end)
            
            continue
            
        
        # Compute background
        background_average_var_freq = get_background_average_var_freq(amplicon_beg, amplicon_end, chrom, coord, counts)
       
        # Compute foreground
        foreground_ref_counts, foreground_var_counts = get_foreground_counts(chrom, coord, counts, ref_base, var_base)
        
        foreground_depth = foreground_ref_counts + foreground_var_counts
        
        p_value = sp.stats.binom.sf(foreground_var_counts - 1, foreground_depth, background_average_var_freq)
        
        out_row = {
                   'chrom' : chrom,
                   'coord' : coord,
                   'ref_base' : ref_base,
                   'var_base' : var_base,
                   'ref_counts' : foreground_ref_counts,
                   'var_counts' : foreground_var_counts,
                   'background_average_var_freq' : background_average_var_freq,
                   'var_freq' : foreground_var_counts / foreground_depth,
                   'p_value' : p_value
                   }
        
        out_row = pd.Series(data=out_row, index=fields)
            
        data.append(out_row)
        
    data = pd.DataFrame(data)
    
    data = compute_corrected_p_values(data, args.family_wise_error_rate, args.low_coverage_threshold)
    
    data.to_csv(args.out_file, sep='\t', index=False, na_rep='NA')
    
def get_background_average_var_freq(amplicon_beg, amplicon_end, chrom, coord, counts):
    background_coords = range(amplicon_beg, amplicon_end + 1)
    
    background_coords.remove(coord)
    
    background_counts = counts[(counts['chrom'] == chrom) & (counts['coord'].isin(background_coords))]
    
    background_ref_counts = background_counts.apply(lambda x: get_counts(x, x['ref_base']), axis=1)
    
    background_var_counts = background_counts.apply(lambda x: get_counts(x, x['var_base']), axis=1)
    
    background_depth = background_ref_counts + background_var_counts
    
    background_ref_counts = background_ref_counts[background_depth > 0]
    
    background_var_counts = background_var_counts[background_depth > 0]
    
    background_depth = background_depth[background_depth > 0]
    
    background_var_freq = background_var_counts / background_depth
    
    background_average_var_freq = background_var_freq.mean()
    
    return background_average_var_freq

def get_counts(x, base):
    if base not in bases:
        return 0
    
    else:
        return x[base]

def get_foreground_counts(chrom, coord, counts, ref_base, var_base):    
    foreground_counts = counts[(counts['chrom'] == chrom) & (counts['coord'] == coord)]

    foreground_ref_counts = foreground_counts[ref_base].values[0]
    
    foreground_var_counts = foreground_counts[var_base].values[0]
    
    return foreground_ref_counts, foreground_var_counts
    
def compute_corrected_p_values(data, family_wise_error_rate, low_coverage_threhsold):
    indices = np.logical_not(pd.isnull(data['p_value']))
    
    p_values = data[indices]['p_value']
    
    variant_present, q_values, _, _ = multipletests(p_values, alpha=family_wise_error_rate, method='fdr_bh')
    
    q_values = pd.Series(data=q_values, index=p_values.index)
    
    data['q_value'] = q_values
    
    # Call variants
    variant_present = pd.Series(data=variant_present, index=p_values.index)
    
    data['variant_present'] = variant_present
    
    data['variant_status'] = data.apply(lambda x: get_variant_status(x, low_coverage_threhsold), axis=1)
    
    data.drop('variant_present', axis=1)
    
    return data

def get_variant_status(x, low_coverage_threshold):
    depth = x['ref_counts'] + x['var_counts']

    if depth >= low_coverage_threshold:
        if x['variant_present'] == True:
            return 'present'

        else:
            return 'absent'

    elif depth == 0:
        return 'no_coverage'

    else:
        return 'low_coverage'

if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('counts_file',
                        help='''Path of file with count data for amplicons.''')
    
    parser.add_argument('positions_file',
                        help='''Path of file with information about target positions and amplicons.''')
    
    parser.add_argument('out_file',
                        help='''Path of where results will be written.''')
    
    parser.add_argument('--family_wise_error_rate', type=float, default=0.01,
                        help='''Family wise error rate call variant present. Default is 0.01.''')
    
    parser.add_argument('--low_coverage_threshold', type=int, default=0,
                        help='''Sites with coverage below this value with be called low coverage. Default is 0.''')    
    
    args = parser.parse_args()
    
    main(args)
