from collections import Counter

import csv
import pysam

def main(args):
    bam_file = pysam.Samfile(args.bam_file, 'rb')
    
    if args.ref_genome:
        ref_genome = pysam.Fastafile(args.ref_genome)
    
    bases, fields = get_bases_and_fields(args)

    writer = csv.DictWriter(open(args.out_file, 'w'), fields, delimiter='\t')
    
    writer.writeheader()
    
    if args.positions_file:
        positions = load_positions(args.positions_file)

        pileup_iterator = positions_iterator(bam_file, positions, args.max_pileup_depth)
        
    else:    
        pileup_iterator = bam_file.pileup(max_depth=args.max_pileup_depth, mask=False, stepper='all')
    
    if args.target_base:
        if args.positions_file is None:
            raise Exception('The --positions_file option must be used with the --target_base option.')
        
        target_bases = load_target_bases(args.positions_file)
    
    for pileup_column in pileup_iterator:
        counts = get_counts(pileup_column,
                            args.min_bqual,
                            args.min_mqual,
                            args.count_duplicates,
                            args.count_qc_failures,
                            args.strand_counts)
        
        total_counts = sum([counts[b] for b in bases])
        
        if total_counts < args.min_counts:
            continue
        
        out_row = {}
        
        out_row['chrom'] = bam_file.getrname(pileup_column.tid)
        
        # One based coordinate
        out_row['coord'] = pileup_column.pos + 1        
        
        if args.ref_base or args.var_base:
            ref_base = ref_genome.fetch(out_row['chrom'], pileup_column.pos, pileup_column.pos + 1)
            
            ref_base = ref_base.upper()
        
        if args.ref_base:
            out_row['ref_base'] = ref_base
            
        if args.var_base:
            out_row['var_base'] = get_var_base(counts, ref_base)
        
        if args.target_base:
            pos = (out_row['chrom'], out_row['coord'])
            
            ref_base = target_bases[pos]['ref_base']
            
            var_base = target_bases[pos]['var_base']
            
            out_row['ref_base'] = ref_base
            
            out_row['var_base'] = var_base
            
            if args.strand_counts:
                out_row['ref_counts_forward'] = counts[ref_base.upper()]
                
                out_row['ref_counts_reverse'] = counts[ref_base.lower()]
                
                out_row['var_counts_forward'] = counts[var_base.upper()]
                
                out_row['var_counts_reverse'] = counts[var_base.lower()]
            
            else:
                out_row['ref_counts'] = counts[ref_base]
                
                out_row['var_counts'] = counts[var_base]
        
        else:
            for base in bases:
                out_row[base] = counts[base]
        
        writer.writerow(out_row)

def get_bases_and_fields(args):
    if args.strand_counts:
        bases = ['A', 'a', 'C', 'c', 'G', 'g', 'T', 't']
    
    else:
        bases = ['A', 'C', 'G', 'T']
    
    if args.target_base:
        if args.ref_base or args.var_base:
            raise Exception('--target_base does not work with --ref_base or --var_base options.')
        
        fields = ['chrom', 'coord', 'ref_base', 'var_base']
        
        if args.strand_counts:
            fields += ['ref_counts_forward', 'ref_counts_reverse',  'var_counts_forward', 'var_counts_reverse']
        
        else:
            fields += ['ref_counts', 'var_counts']
    else:
        extra_fields = []
        
        if args.ref_base:
            if args.ref_genome is None:
                raise Exception('--ref_genome flag must be set to use --ref_base flag.')
            
            extra_fields.append('ref_base')
            
        if args.var_base:
            if args.ref_genome is None:
                raise Exception('--ref_genome flag must be set to use --var_base flag.')
            
            extra_fields.append('var_base')        
        
        fields = ['chrom', 'coord'] + extra_fields + bases
    
    return bases, fields  

def get_counts(pileup_column, min_bqual, min_mqual, count_duplicates, count_qc_failures, strand_counts):
    bases = []
    
    for pileup_read in pileup_column.pileups:
        # Skip flagged duplicates if we don't want them
        if pileup_read.alignment.is_duplicate and not count_duplicates:
            continue
        
        # Skip QC failures if we don't want them
        if pileup_read.alignment.is_qcfail and not count_qc_failures:
            continue
        
        if pileup_read.is_del:
            continue
        
        mqual = pileup_read.alignment.mapq
        
        # Skip positions with low mapping quality
        if mqual < min_mqual:
            continue

        # Nucleotide handling
        else:
            bqual = ord(pileup_read.alignment.qual[pileup_read.qpos]) - 33
            
            if bqual < min_bqual:
                continue 
            
            base = pileup_read.alignment.seq[pileup_read.qpos]
            
            if strand_counts:
                if pileup_read.alignment.is_reverse:
                    base = base.lower()
                
                else:
                    base = base.upper()
            
            else:
                base = base.upper()
            
            bases.append(base)
    
    return Counter(bases)

def get_var_base(counts, ref_base):
    # Get rid of strand information
    counts = Counter([x.upper() for x in counts.elements()])
    
    # Remove reference base
    del counts[ref_base]
    
    if len(counts) == 0:
        var_base = 'N'
    
    else:
        # Get most common non-reference base
        var_base, var_counts = counts.most_common()[0]

    return var_base

def load_positions(file_name):
    positions = []
    
    reader = csv.DictReader(open(file_name), delimiter='\t')
    
    for row in reader:
        positions.append((row['chrom'], int(row['coord'])))
    
    return positions

def load_target_bases(file_name):
    target_bases = {}
    
    reader = csv.DictReader(open(file_name), delimiter='\t')
    
    if 'ref_base' not in reader.fieldnames or 'var_base' not in reader.fieldnames:
        raise Exception('File {0} must have ref_base and var_base fields to use --target_base option.'.format(file_name))
    
    for row in reader:
        pos = (row['chrom'], int(row['coord']))
        
        target_bases[pos] = {'ref_base' : row['ref_base'], 'var_base' : row['var_base']}
        
    return target_bases       

def positions_iterator(bam_file, positions, max_pileup_depth):
    for chrom, coord in positions:
        if bam_file.count(chrom, coord-1, coord) == 0:
            pos = coord - 1
            
            tid = bam_file.gettid(chrom)
            
            pileup_column = DummyPileupProxy(pos, tid)
        
        else:
            pileup_iterator = bam_file.pileup(chrom, 
                                              coord - 1, 
                                              coord, 
                                              truncate=True, 
                                              max_depth=max_pileup_depth, 
                                              mask=False,
                                              stepper='all')
            
            for pileup_column in pileup_iterator:
                break
        
        yield pileup_column

class DummyPileupProxy(object):
    def __init__(self, pos, tid):
        self.pileups = []
        
        self.pos = pos
        
        self.tid = tid

if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('bam_file',
                        help='''BAM file to generate counts from.''')
    
    parser.add_argument('out_file',
                        help='''Path where counts file will be written.''')
    
    parser.add_argument('--count_duplicates', default=False, action='store_true',
                        help='''If set flagged duplicates will be counted. Default is to ignore duplicates.''')
    
    parser.add_argument('--count_qc_failures', default=False, action='store_true',
                        help='''If set flagged QC failures will be counted. Default is to ignore QC failures.''')

    parser.add_argument('--max_pileup_depth', type=int, default=int(1e7),
                        help='''Max coverage depth of a position pileup engine will consider. Default is 1e7.''')
    
    parser.add_argument('--min_bqual', type=int, default=0,
                        help='''Minimum base quality required to count a read. Default 0.''')

    parser.add_argument('--min_counts', type=int, default=0,
                        help='''Minimum number of counts required to report a site. Default is 0.''')

    parser.add_argument('--min_mqual', type=int, default=0,
                        help='''Minimum mapping quality required to count a read. Default 0.''')
    
    parser.add_argument('--positions_file', default=None,
                        help='''Tab delimited file with head which lists the positions to get counts from. File must
                        contain `chrom` and `coord` fields which specify the chromsome and chromosome coordinates
                        respectively for the positions. Optionally ref_base and var_base fields can be used in
                        conjuction with the target_base option. If not specified all positions with coverage will be
                        reported.''')
    
    parser.add_argument('--ref_base', default=False, action='store_true',
                        help='''If set an extra field, ref_base, will be added to the file which reports the base in the
                        reference genome at each positions. Default is not report this. Note you must set the ref_genome
                        flag for this to work.''')
    
    parser.add_argument('--ref_genome', default=None,
                        help='''Path to reference genome in FASTA format. The reference genome should have a .fai index
                        file in the same folder. This option needs to be set when using the ref_base and var_base
                        options.''')
    
    parser.add_argument('--strand_counts', default=False, action='store_true',
                        help='''If set the strand of each base will be counted. Instead of four fields (A, C, G, T),
                        eight fields (A, a, C, c, G, g, T, t) will be reported where upper case values are the forward
                        strand and lower case values are the reverse strand. Default is to ignore strand.''')
    
    parser.add_argument('--target_base', default=False, action='store_true',
                        help='''If set counts from specific bases will be given as ref_counts, var_counts. This options
                        requires that the positions_file option has been set and the file contains the fields ref_base
                        and var_base.''')
    
    parser.add_argument('--var_base', default=False, action='store_true',
                        help='''If set an extra field, var_base, will be added to the file which reports the most common
                        non-reference base at each positions. Default is not report this. Note you must set the
                        ref_genome flag for this to work.''')
    
    args = parser.parse_args()
    
    main(args)
