import csv
from sys import getsizeof

def main(args):
    reader = csv.DictReader(open(args.in_file), delimiter='\t')
    
    positions = set()
    
    for row in reader:
        chrom = row['chrom']
        
        beg = int(row['amplicon_beg'])
        
        end = int(row['amplicon_end'])
        
        for coord in xrange(beg, end+1):
            positions.add((chrom, coord))

    getsizeof(positions)
    out_rows = []
    
    for chrom, coord in sorted(positions):
        out_row = {'chrom' : chrom, 'coord' : coord}
        
        out_rows.append(out_row)
    
    writer = csv.DictWriter(open(args.out_file, 'w'), ['chrom', 'coord'], delimiter='\t')
    
    writer.writeheader()
        
    writer.writerows(out_rows)
    
if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('in_file',
                        help='''Path of file listing amplicon start and end points under fields amplicon_beg,
                        amplicon_end''')
    
    parser.add_argument('out_file',
                        help='''Path where output file will be written.''')
    
    args = parser.parse_args()
    
    main(args)
