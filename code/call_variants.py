'''
Pipeline to call variants using GATK

This code can be used as it is and it is a part of the Targeted Sequencing pipeline (discovery mode)


__author__ = "Rad"
__copyright__ = "Copyright 2014, Shah Lab"
__credits__ = ["Rad", "Ali", "Sohrab", "Karey", "Diljot"]
__license__ = "Don't know yet"
__version__ = "0.0.1"
__maintainer__ = "Rad"
__email__ = "raniba@bccrc.ca"
__status__ = "Development"

'''
from pipelines.io import make_directory, make_parent_directory
from pipeline_notifications import notify_user
from ruffus import *
from ruffus.ruffus_utility import CHECKSUM_FILE_TIMESTAMPS
from glob import *
import subprocess
from subprocess import check_output
from subprocess import Popen, PIPE
import argparse
import os,sys,json, csv , operator
import yaml
import time
from termcolor import colored
import functools
import logging

#=======================================================================================================================
# Read Command Line Input
#=======================================================================================================================



parser = argparse.ArgumentParser(description="Pipeline to call variants using GATK")

parser.add_argument('--input_dir',
                    help='''Input is a directory containing Bam files to be realigned''')

parser.add_argument('--output_dir',
                    help='''Output is a directory containing subfolders for targets, realignments, deduplicated bam files etc''')

parser.add_argument('--config_file',
                   help='''Path to yaml config file.''')

parser.add_argument('--num_cpus', type=int, default=1,
                    help='''Number of cpus to use for the analysis. If set to -1 then as many cpus as samples will
                    be used. Default is 1.''')

parser.add_argument('--dedup', choices=['yes', 'no'], default='yes',
                    help='''Read deduplication to remove any noise in the alignment that may come from
                    PCR duplicate reads, this can reduce the size of the alignment. If you are doing a
                    deep sequencing analysis you may want to turn this to No ''')

parser.add_argument('--mode', choices=['local', 'cluster', 'printout'], default='printout',
                    help='''Mode to run the pipeline in. local will run the pipeline on the compute it was launched
                    from. cluster will submit the jobs to a cluster using SGE. printout shows which tasks will be
                    run. default is printout.''')

parser.add_argument('--targets',
                    help='''A file containing target positions to be used to filter all calls within the ranges of amplicon starts and ends''')


parser.add_argument('--email',default="",
                    help='''If a valid email address is provided, users will be notified at the begining and at the end of the job''')


args = parser.parse_args()


fh = open(args.config_file)
config = yaml.load(fh)
fh.close()


gatk  = config['gatk']
picard = config['picard']
reference = config['reference']
dbsnp  = config['dbsnp']
indels = config['indels']
java = config['java']


mandatory_env = [gatk, picard, reference, dbsnp, indels, java]

data = {
'Input directory' : args.input_dir ,
'Output directory' : args.output_dir ,
'Targets Positions' : args.targets ,
'Config file' : args.config_file ,
'Deduplication step' : args.dedup ,
'Run mode' : args.mode ,
'Number of CPUs used' : args.num_cpus ,
}

#==================
# Prepare Logging (testing logging for local jobs)
#==================

logging.basicConfig(filename='call_variants.log',
                            filemode='w',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

#====================
# Pipeline utilities
#====================

def timer(stream=sys.stdout):
    """The timer decorator wraps a function and prints elapsed time to standard
    out, or any other file-like object with a .write() method.
    """
    def actual_timer(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            # Start the timer.
            start = time.time()
            # Run the decorated function.
            ret = func(*args, **kwargs)
            # Stop the timer.
            end = time.time()
            elapsed = end - start
            name = func.__name__
            stream.write(colored("{} took {} seconds\n".format(name, elapsed), 'red', attrs=['bold'] ))
            # Return the decorated function's return value.
            return ret
        return wrapper
    return actual_timer


def check_config_params():
  for env in mandatory_env:
    if env.__len__() == 0 :
      print >> sys.stderr, "It seems that one third party tool is not defined in the config file, please check your config file"
      sys.exit(1)


def success():
  print >> sys.stderr , colored('   Task successfully finished   ', 'white', 'on_red')



def sort_tmp_results(outputfile, tempfile):

  with open(outputfile,"w") as res:
    unique = []
    intersected = open(tempfile,'r')
    intersected_tosort = csv.reader(intersected,delimiter="\t")
    intersect_sorted = sorted(intersected_tosort, key=operator.itemgetter(0,1))
    for line in intersect_sorted:
      if line not in unique:
        unique.append(line)
        res.write("\t".join(line))
        res.write("\n")
  os.remove(tempfile)


#=======================================================================================================================
# Start the Realignment pipeline
#=======================================================================================================================



check_config_params()

if args.email :

    SUBJ = "Your variant calling pipeline is up and running"
    MESS = "Hi There, you successully fired up a variant calling pipeline, your analysis is up and running ! \n"+ \
                "Your analysis report (in json format, do what you want with it), you will be notified when the analysis is over.\n"+json.dumps([data], indent=2)+ \
                "\nCheers !\nThe pipeline :) (don't try to reply, am not a human)\n"

    notify_user(args.email, SUBJ ,MESS, "DeepSequencingPipeline");



@timer(sys.stderr)
@posttask(success)
def load_bam_files():
  #bam_files = glob(os.path.join(os.path.normpath(".",args.input_dir, '*.bam'))
  try:
    for path,subdir,files in os.walk(args.input_dir):
      for file in files :
        if file.endswith(".bam"):
          in_file = os.path.abspath(os.path.join(args.input_dir,file))
          #rehead_bam_file(in_file, in_file.replace(".bam","_reh.bam"))
          #reheaded = in_file.replace(".bam","_reh.bam")
          out_file = os.path.abspath(os.path.join(args.output_dir,"bam/",file)).replace(".bam",".sorted.bam")
          yield [in_file,out_file]
    logging.info("Files successfully loaded")
  except:
    logging.error("Problem while loading bam files")

@posttask(success)
@files(load_bam_files)
@timer(sys.stderr)
def sort_mapped_reads(in_file, out_file):
    '''
    Sort the bam files using Samtools sort (room for using Picard)
    '''


    make_parent_directory(out_file)

    out_prefix = out_file.replace(".bam", "")
    cmd = 'samtools'

    cmd_args = ['sort', in_file, out_prefix]

    run_cmd(cmd, cmd_args, max_mem=16)



### Switch set to Yes -> Do deduplication

@posttask(success)
@active_if(args.dedup == 'yes')
@transform(sort_mapped_reads, regex(r'(.*)/bam/(.*)\.bam'), r"\1/dedup/\2.dedup.bam")
@timer(sys.stderr)
def deduplicate(bam_sorted, bam_deduplicated):
  '''
  Remove PCR amplification identical reads
  '''


  make_parent_directory(bam_deduplicated)

  cmd = java
  cmd_args = ["-Xmx4g", "-jar", os.path.join(picard,"MarkDuplicates.jar"),
             ''.join(["INPUT=",bam_sorted]),
             ''.join(["OUTPUT=",bam_deduplicated]),
             "METRICS_FILE=metrics.txt"
              ]
  run_cmd(cmd,cmd_args, max_mem=16)


@posttask(success)
@active_if(args.dedup == 'yes')
@transform(deduplicate, regex(r'(.*)/dedup/(.*)\.dedup\.bam'), '.bai')
@timer(sys.stderr)
def index_mapped_reads(in_file, out_file):
    '''
    Sort the bam files using Samtools sort (room for using Picard)
    '''

    cmd = 'samtools'

    cmd_args = ['index', in_file]

    run_cmd(cmd, cmd_args, max_mem=16)


@posttask(success)
@active_if(args.dedup == 'yes')
@follows(index_mapped_reads)
@transform(deduplicate, regex(r'(.*)/dedup/(.*)\.dedup\.bam'), r'\1/intervals/\2.intervals')
@timer(sys.stderr)
def create_targets(in_file, out_file):
  '''
  GATK RealignerTargetCreator : Create suspecious regions where a realignment will be done
  '''

  make_parent_directory(out_file)


  cmd = java
  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
             "-T","RealignerTargetCreator",
             "-R",config['reference'],
             "-I",in_file,
             "-o",out_file
             ]

  run_cmd(cmd, cmd_args, max_mem=16)




@posttask(success)
@active_if(args.dedup=='yes')
@transform(create_targets, regex(r'(.*)/intervals/(.*)\.intervals'), add_inputs(r'\1/dedup/\2.dedup.bam'),r'\1/realigned/\2.realigned.bam')
@timer(sys.stderr)
def realign(in_files, out_file):
  '''
  GATK IndelRealigner
  '''


  make_parent_directory(out_file)

  cmd = java

  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
           "-T","IndelRealigner",
           "-R",config['reference'],
           "-targetIntervals",in_files[0],
           "-I",in_files[1],
           "-o",out_file,
           "-LOD", 1.0
           ]
  run_cmd(cmd, cmd_args, max_mem=16)


### Swithch set to No -> Do NOT deduplicate


@posttask(success)
@active_if(args.dedup == 'no')
@follows(sort_mapped_reads)
@transform(sort_mapped_reads, regex(r'(.*)/bam/(.*)\.sorted\.bam'), '.bai')
@timer(sys.stderr)
def index_mapped_reads_ndp(in_file, out_file):
    '''
    Sort the bam files using Samtools sort (room for using Picard)
    '''

    cmd = 'samtools'

    cmd_args = ['index', in_file]

    run_cmd(cmd, cmd_args)


@posttask(success)
@active_if(args.dedup == 'no')
@follows(index_mapped_reads_ndp)
@transform(sort_mapped_reads, regex(r'(.*)/bam/(.*)\.sorted\.bam'), r'\1/intervals/\2.intervals')
@timer(sys.stderr)
def create_targets_ndp(in_file, out_file):
  '''
  GATK RealignerTargetCreator : Create suspecious regions where a realignment will be done
  '''

  make_parent_directory(out_file)


  cmd = java
  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
             "-T","RealignerTargetCreator",
             "-R",config['reference'],
             "-I",in_file,
             "-o",out_file
             ]

  run_cmd(cmd, cmd_args, max_mem=16)


@posttask(success)
@active_if(args.dedup=='no')
@transform(create_targets_ndp, regex(r'(.*)/intervals/(.*)\.intervals'), add_inputs(r'\1/bam/\2.sorted.bam'),r'\1/realigned/\2.realigned.bam')
@timer(sys.stderr)
def realign_ndp(in_files, out_file):
  '''
  GATK IndelRealigner
  '''


  make_parent_directory(out_file)

  cmd = java

  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
           "-T","IndelRealigner",
           "-R",config['reference'],
           "-targetIntervals",in_files[0],
           "-I",in_files[1],
           "-o",out_file
           ]
  run_cmd(cmd, cmd_args, max_mem=16)



### Now we decide what road to go, realign or realign_ndp

if(args.dedup == 'yes'):
  realignment_switcher = output_from("realign")
else:
  realignment_switcher = output_from("realign_ndp")

### End Switch ###

@posttask(success)
@transform(realignment_switcher,regex(r'(.*)/realigned/(.*)\.realigned\.bam'),r'\1/realigned/\2.realigned.grp')
@timer(sys.stderr)
def base_quality_recalibrator_pre(in_file, out_file):
  '''
  GATK recalibrate 1

  '''
  #recalibrate(in_file, out_file)

  cmd = java

  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
             "-T","BaseRecalibrator",
             "-R",config['reference'],
             "-knownSites", config['dbsnp'],
 #            "-knownSites", config['indels'],
             "-I",in_file,
             "-o",out_file,
             #"-plots","recal.grp.pdf"
             ]

  run_cmd(cmd, cmd_args, max_mem=16)



@posttask(success)
@follows(base_quality_recalibrator_pre)
@transform(realignment_switcher,regex(r'(.*)/realigned/(.*)\.realigned\.bam'), add_inputs(r'\1/realigned/\2.realigned.grp') , r'\1/realigned/\2.post_recal.grp2')
@timer(sys.stderr)
def base_quality_recalibrator_post(in_files, out_file):
  '''
  GATK recalibrate 2nd step
  '''
  #recalibrate(in_file, out_file)

  cmd = java

  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
             "-T","BaseRecalibrator",
             "-R",config['reference'],
             "-I",in_files[0],
             "-BQSR", in_files[1],
             "-knownSites", config['dbsnp'],
             "-o",out_file,
             #"-plots","post_recal.grp.pdf"
             ]

  run_cmd(cmd, cmd_args, max_mem=16)





@posttask(success)
@follows(base_quality_recalibrator_post)
@transform(base_quality_recalibrator_pre, regex(r'(.*)/realigned/(.*)\.grp'), add_inputs(r'\1/realigned/\2.bam'),r'\1/realigned/\2.recalibrated.bam')
@timer(sys.stderr)
def print_reads(in_files, out_file):
  '''
  GATK Get the recalibrated alignment
  '''
  #print_reads(in_file,out_file)

  cmd = java

  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
             "-T","PrintReads",
             "-R",config['reference'],
             "-I",in_files[1],
             "-BQSR", in_files[0],
             "-o",out_file
             ]

  run_cmd(cmd, cmd_args, max_mem=16)




@posttask(success)
@transform(print_reads, regex(r'(.*)/realigned/(.*)\.recalibrated\.bam'),r'\1/realigned/\2.sorted.bam')
@timer(sys.stderr)
def sort_recalibrated_bams(in_file, out_file):
  '''
  Sort the bam files using Samtools sort (room for using Picard)
  '''
  #sort_recalibrated_alignment(in_file,out_file)

  out_prefix = out_file.replace(".bam","")
  cmd = 'samtools'

  cmd_args = ['sort', in_file, out_prefix]

  run_cmd(cmd, cmd_args)




@posttask(success)
@transform(sort_recalibrated_bams,regex(r'(.*)/realigned/(.*)\.sorted\.bam'),'.bai')
@timer(sys.stderr)
def index_recalibrated_bams(in_file, out_file):
  '''
  Index the bam files using Samtools index (room for using Picard)
  '''
  #index_recalibrated_alignment(in_file,out_file)

  cmd = 'samtools'

  cmd_args = ['index', in_file]

  run_cmd(cmd, cmd_args)


'''
  This is the difference with the indel_realignment_pipeline
  In the following secion we will add a couple of other steps from the GATK protocol
  We need to call variants (SNPs, Indels, SV) using HaplotypeCaller for better accuracy
  I anticipate UnifiedGenotyper to be discontinued in the future, So I prefer using HC and upgrading later
  So what we will be adding here :

   - Call HaplotypeCaller
       INPUT  : realigned Bam file
       OUTPUT : VCF
   - Generate VCFs for each Bam file
   - Merge VCFs for all the samples
       INPUT  : VCFs
       OUTPUT : One VCF
   - Convert the merged VCF to a bed file (<experiment_id>_positions.tsv)

'''


@posttask(success)
@follows(index_recalibrated_bams)
@transform(sort_recalibrated_bams, regex(r'(.*)/realigned/(.*)\.sorted\.bam'),r'\1/calls/\2.vcf')
@timer(sys.stderr)
def call_variant(in_file, out_file):
  '''
  Call HaplotypeCaller with its default parameters
  '''
  make_parent_directory(out_file)
  cmd = java
  cmd_args = ["-Xmx4g", "-jar", os.path.join(gatk,"GenomeAnalysisTK.jar"),
             "-T","HaplotypeCaller",
             "-R",config['reference'],
             "-I",in_file,
             "-o",out_file,
             "-stand_call_conf", 30,
             "-stand_emit_conf", 10,
             "-minPruning", 3
             ]

  run_cmd(cmd, cmd_args, max_mem=16)


@posttask(success)
#@transform(call_variant, regex(r'(.*)/calls/(.*)\.vcf'), r'\1/calls/merged_variants.vcf')
@merge(call_variant, r'{0}/merged_calls/{1}.vcf'.format(args.output_dir, 'merged_variants'))
@timer(sys.stderr)
def merge_vcfs(vcf_files, merged_vcf_file):
  make_parent_directory(merged_vcf_file)
  with open(merged_vcf_file, 'a') as ofile:
    for vcf_file in vcf_files:
      vf = open(vcf_file,'r')
      lines = vf.readlines()
      for l in lines:
        if l.startswith('#'):
          continue
        else:
          print >> ofile,l


@posttask(success)
@transform(merge_vcfs, regex(r'(.*)/merged_calls/(.*)\.vcf'),r'\1/positions/\2_positions.tsv')
@timer(sys.stderr)
def create_positions(merged_vcf, positions_file):
  make_parent_directory(positions_file)
  # line to be parsed :
  #20     14370   rs6054257 G      A       29   PASS   NS=3;DP=14;AF=0.5;DB;H2           GT:GQ:DP:HQ 0|0:48:1:51,51 1|0:48:8:51,51 1/1:43:5:.,.
  with open(positions_file,'a') as pos_file:
    with open(merged_vcf,'r') as merge_file:
      for line in merge_file :
        if len(line) > 1 :
          columns = line.split("\t")
          outline = [chrom, position, name, ref_allele, alt_allele] = [columns[0],columns[1],columns[2],columns[3],columns[4]]
          pos_file.write("\t".join(outline[i] for i in range(5)))
          pos_file.write("\n")


# We need now to filter out all positions out of the target positions
# The targets positions should be in tabular format. Example :
# <chr>  <amplicon_start>  <amplicon_end>

@posttask(success)
@transform(create_positions, regex(r'(.*)/positions/(.*)\.tsv'), add_inputs(args.targets), r'\1/positions/\2_filtered.tsv' )
def filter_positions_in_amplicon(coordinates, result):

  merged_positions = coordinates[0]
  positions_file = coordinates[1]
  with open(result+"_tmp",'w') as intersect:
    with open(merged_positions,'r') as merge_file:
      for line in merge_file :
        if len(line) > 1 :
          columns = line.strip().split("\t")
          #print columns
          outline = [chrom, position, name, ref_allele, alt_allele] = [columns[0],columns[1],columns[2],columns[3],columns[4]]
          candidate_position = position

          if(len(columns[3])==1 and len(columns[4])==1):
            with open(positions_file, 'r') as pos_file:
              for posline in pos_file :
                if len(posline) > 1 :
                  pos_columns = posline.strip().split("\t")
                  pos_outline = [pos_chrom, amplicon_start, amplicon_end] = [pos_columns[0],pos_columns[1], pos_columns[2]]
                  if(candidate_position > amplicon_start) and (candidate_position < amplicon_end):
                    #print "found"
                    row = [chrom, candidate_position, ref_allele, alt_allele, amplicon_start, amplicon_end]
                    intersect.write("\t".join(row[i] for i in range(6)))
                    intersect.write("\n")

  sort_tmp_results(result, result+"_tmp")


@follows(filter_positions_in_amplicon)
def end():
    pass


#=======================================================================================================================
# Run pipeline
#=======================================================================================================================

if args.mode in ['cluster', 'local']:
    if args.mode == 'cluster':
        from pipelines.job_manager import ClusterJobManager

        import datetime

        log_dir = os.path.join(args.output_dir, 'log', datetime.datetime.now().isoformat(','))

        job_manager = ClusterJobManager(log_dir)

    elif args.mode == 'local':
        from pipelines.job_manager import LocalJobManager

        job_manager = LocalJobManager()

    run_cmd = job_manager.run_job

    try:
#        pipeline_run([base_quality_recalibrator_post,print_reads,sort_recalibrated_bams,index_recalibrated_bams], multiprocess=args.num_cpus) #, use_multi_threading=True)
       	pipeline_run(end, multithread=args.num_cpus, checksum_level=CHECKSUM_FILE_TIMESTAMPS)


    finally:
        job_manager.close()

elif args.mode == 'printout':
    import sys

    pipeline_printout(sys.stdout, end, checksum_level=CHECKSUM_FILE_TIMESTAMPS , verbose=3, wrap_width=200)

if args.email :

    SUBJ = "Your variant calling analysis is done"
    MESS = "Hi There, your job is done, please review the result directory for sanity check"

    notify_user(args.email, SUBJ ,MESS, "DeepSequencingPipeline");


#pipeline_printout_graph ("gatk_variant_call_pipeline.png", "png", end,test_all_task_for_update = False, pipeline_name="GATK Variant Call pipeline", user_colour_scheme = {"colour_scheme_index" :6})
#pipeline_printout(sys.stdout, end, verbose=3, wrap_width=200)
