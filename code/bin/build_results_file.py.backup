from __future__ import division

from collections import OrderedDict

import os
import pandas as pd

def main(args):
    prefix = os.path.commonprefix(args.sample_ids)

    data = OrderedDict()
    
    writer = pd.io.excel.ExcelWriter(args.out_file)
    
    for sample_id, file_name in zip(args.sample_ids, args.variant_call_files):
        df = pd.read_csv(file_name, sep='\t')
        
        # Trim of common prefix since we will have to truncate long sample_ids
        sheet_name = sample_id.replace(prefix, '')
        
        # Write to Excel file
        if sample_id == args.normal_sample:
            sheet_name += ' (normal_sample)'
        
        # Handle excel hard limit of 31 characters on sheet names
        if len(sheet_name) > 31:
            sheet_name = sheet_name[:31]
        
        df.to_excel(writer, sheet_name=sheet_name, na_rep='NA', index=False)
        
        # Re-index to merge
        df = df.set_index(['chrom', 'coord'])
        
        data[sample_id] = df
    
    if args.normal_sample is not None:
        summary_data = get_summary_data(data, args.normal_sample)
        
        summary_data.to_excel(writer, sheet_name='summary', na_rep='NA', index=False)
    
    writer.save()
    
def get_summary_data(data, normal_sample):
    summary_data = []
    
    for index, normal_row in data[normal_sample].iterrows():
        out_row = {
                   'chrom' : index[0],
                   'coord' : index[1],
                   'ref_base' : normal_row['ref_base'],
                   'var_base' : normal_row['var_base'],
                   'normal_ref_counts' : normal_row['ref_counts'],
                   'normal_var_counts' : normal_row['var_counts'],
                   }
        
        normal_variant_status = normal_row['variant_status']
        
        normal_depth = out_row['normal_ref_counts'] + out_row['normal_var_counts']
        
        if normal_depth > 0:
            normal_var_freq = out_row['normal_var_counts'] / normal_depth
        
        for sample in data:
            if sample == normal_sample:
                continue
                
            sample_row = data[sample].ix[index]
            
            out_row['{0}_ref_counts'.format(sample)] = sample_row['ref_counts']
            
            out_row['{0}_var_counts'.format(sample)] = sample_row['var_counts']
            
            sample_variant_status = sample_row['variant_status']
            
            if normal_depth == 0:
                sample_somatic_status = 'unknown'
            
            else:
                sample_somatic_status = call_somatic_status(normal_variant_status,
                                                            sample_variant_status,
                                                            normal_var_freq,
                                                            args.min_normal_germline_var_freq)
            
            out_row['{0}_somatic_status'.format(sample)] = sample_somatic_status
            
        summary_data.append(out_row)

    columns = ['chrom', 'coord', 'ref_base', 'var_base', 'normal_ref_counts', 'normal_var_counts']
    
    for sample in data:
        if sample == normal_sample:
            continue
        
        columns.append('{0}_ref_counts'.format(sample))
        
        columns.append('{0}_var_counts'.format(sample))
        
        columns.append('{0}_somatic_status'.format(sample))
            
            
    summary_data = pd.DataFrame(summary_data, columns=columns)
    
    return summary_data
    
def call_somatic_status(normal_variant_status, sample_variant_status, normal_var_freq, min_normal_germline_var_freq):    
    if normal_variant_status == 'present':
        if normal_var_freq < min_normal_germline_var_freq:
            if sample_variant_status == 'absent':
                sample_somatic_status = 'wildtype'
            
            elif sample_variant_status == 'present':
                sample_somatic_status = 'somatic'
            
            else:
                sample_somatic_status = 'unknown'
        
        else:
            sample_somatic_status = 'germline'
                
    elif normal_variant_status == 'absent':
        if sample_variant_status == 'absent':
            sample_somatic_status = 'wildtype'
        
        elif sample_variant_status == 'present':
            sample_somatic_status = 'somatic'
        
        else:
            sample_somatic_status = 'unknown'
    
    else:
        sample_somatic_status = 'unknown'
    
    return sample_somatic_status
                    
if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument('out_file',
                        help='''Path where results will be written in .xls format.''')
    
    parser.add_argument('--sample_ids', nargs='+', required=True,
                        help='''Sample ID for each file passed to --variant_call_files flag in the same order.''')
    
    parser.add_argument('--variant_call_files', nargs='+', required=True,
                        help='''List of files with counts and variant call status.''')
    
    parser.add_argument('--normal_sample', default=None,
                        help='''Identify which sample to use as control when calling somatic status.''')
    
    parser.add_argument('--min_normal_germline_var_freq', default=0.2,
                        help='''Minimum variant allelic frequency in tumour to call the site germline. This is used to
                        reject positions which pass the Binomial exact test in the normal but are present at low variant
                        allelic frequencies which is likely not a germline. Default is 0.2.''')
    
    args = parser.parse_args()
    
    main(args)
